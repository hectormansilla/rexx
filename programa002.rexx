/* Ejemplo del uso de la sentencia de control IF, ELSE.
La función DATATYPE verifica si lo que se ingreso mediante el INPUTfunction
es o no un numero */

say 'Ingrese algo...'
pull input

if datatype(input,N) then
say 'Su ingreso es numerico'
else
say 'Su ingreso no es numerico'

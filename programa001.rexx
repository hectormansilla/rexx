/********************
* Adivina el numero *
*********************/

el_numero = random(1,10)
say "Estoy pensando en un numero entre 1 y 10 ¿Cual Sera?"
pull cual_es

if el_numero = cual_es then
  say "Acertaste!"
else
  say "Lo siento, el numero que tenía en mente era: " el_numero

  say "Adios!"
